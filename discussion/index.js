// console.log("Hello")

// new section: What are objects?

//=> An object is a collection of related data  and/or functionality

//Analogy:
// in CSS ==> everything is "box"
//in JS ==> most things are objects
//Example: Cellphone --> an object  in the real world
	//it has its own properties (color, weight, unit)
	//it has its own functions (open, close, send messages)

//how to create / instantiate an object in JS
//2 ways to create an object
//1. Curly braces
	// let cellphone = {}
//2. Object constructor / object initializer
	//Object()
	// let cellphone = Object()

	// let cellphone = {
	// 	color: "White",
	// 	weight: "115 grams",
	// 	//an object can also contain functions
	// 	alarm: function() {
	// 		console.log('Alarm is Buzzing');
	// 	},
	// 	ring: function() {
	// 		console.log('Cellphone is Ringing');
	// 	}
	// }

	// console.log(cellphone);

	// let cellphone = Object({
	// 	color: "White"
	// 	weight: '115 grams'
	// 	alarm: function() {
	// 		console.log ('Alarm is Buzzing');
	// 	}
	// 	ring: fuction () {
	// 		console.log('Cellphone is Ringing');
	// 	}
	// })
	// console.log(cellphone);

	// // Babel
	//--> an example of a transpiler

	//How to create an object as a blueprint?
	//--we can create reusable functions that will create several objects that have the same data structure


//this approach is very useful in creating multiple instances/duplicates/copies of the same object

//syntax: function DesiredBlueprintName(arguments) {
	// this.arguments = valueNiargument
// }

	// function Laptop(name, manufactureDate, color) {
	// 	this.pangalan = name;
	// 	this.createdOn = manufactureDate;
	// 	this.kulay = color
	// }
	// //this ==> keyword allows us to assign new object properties by associating the received values

	// //new --> this keywword creates an instance of a new object
	// Let item1 = new Laptop("Lenovo","2008", "black");
	// Let item2 = new Laptop("Asus", "2015", "Pink");

	// console.log(item1);
	// console.log(item2);


	function PokemonAnatomy(name, type, level){
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonType = type;
		this.pokeHealth = 80 * level;
		this.attack = function() {
			console.log("Pokemon Tackle");
		}
	}

	let  pikachu = new PokemonAnatomy('Pikachu', 'Electric', 3);
	let  ratata = new PokemonAnatomy('Ratata', 'Ground', 2);
	let  onyx = new PokemonAnatomy('Onyx','Rock', 8);
	let  meowth = new PokemonAnatomy('Meowth','Normal', 9);
	let  snorlax = new PokemonAnatomy('Snorlax','Normal',9);

	console.log(pikachu)

//access elements/properties inside an object
//1. Dot notation (.)
console.log(pikachu.pokeHealth);

//Square brackets []
console.log(snorlax['pokemonType']);